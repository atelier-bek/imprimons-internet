# Imprimons internet
## workshop au 75
Mettre en forme du contenu web pour l'impression. La finalité est une édition imprimée en riso.  
[le pad](https://semestriel.framapad.org/p/workshop75)

## outil
Un outil d'édition dans le navigateur est mis en place pour l'occasion. Il utilise des pads et permet une prévisualisation en temps réel de la double-page.
![image](exemple/preview.png)

end
test3
