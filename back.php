<?php

  $page       = $_GET['page'];
  $slug       = $_GET['slug'];

  $background_html = file_get_contents('back/'. $slug .'/'. $slug .'.html');
  $background_css = file_get_contents('back/'. $slug .'/'. $slug .'.css');

  $result     = explode('<!-- BREAK -->', $background_html);

  if ($page == 'gauche'){
    $result   = $result[0];
  } else if ($page == 'droite'){
    $result   = $result[1];
  }

  include('include/header.php');
?>

    <style>
      @media print, screen {
        <?= $background_css ?>
      }
    </style>

    <?= $result ?>

  </body>
</html>
