<style media="screen, print">

  body{
    margin: 0;
    padding: 0;
  }

  @page{
    size: A4 !important;
    margin: 0;
    padding: 0;
  }

  iframe{
    width: 19.5cm;
    height: 27.5cm;
    margin-top: 1.1cm;
    border: 1px solid black;
    box-sizing: border-box;
    -moz-box-sizing: border-box;
    -webkit-box-sizing: border-box;
  }

  .pages:nth-of-type(even) iframe{
    margin-right: 1.5cm;
  }

  .pages:nth-of-type(odd) iframe{
    margin-left: 1.5cm;
  }

  .pageG,
  .pageD{
    -webkit-page-break-after: always;
    page-break-after: always;
    width: 21cm;
    height: 29.7cm;
  }

  .pageG:nth-of-type(1){
    /*display: none;*/
  }

  .cropTop,
  .cropBottom{
    position: absolute;
    width: 19.5cm;
    height: 0.3cm;
    margin-right: 1.5cm;
    border-left: 0.05cm solid black;
    border-right: 0.05cm solid black;
    box-sizing: border-box;
    -moz-box-sizing: border-box;
    -webkit-box-sizing: border-box;
  }

  .cropLeft,
  .cropRight{
    position: absolute;
    width: 0.3cm;
    height: 27.5cm;
    margin-top: 1.1cm;
    border-top: 0.05cm solid black;
    border-bottom: 0.05cm solid black;
    box-sizing: border-box;
    -moz-box-sizing: border-box;
    -webkit-box-sizing: border-box;
  }

  .cropTop{
    margin-top: 0.5cm;
  }

  .cropBottom{
    margin-top: 0.3cm;
  }

  .pages:nth-of-type(odd) .cropBottom,
  .pages:nth-of-type(odd) .cropTop{
    left: 1.5cm;
  }

  .cropLeft{
    margin-left: 0.8cm;
  }

  .cropRight{
    margin-left: 19.8cm;
  }

</style>
<style media="print">

  iframe{
    border: none;
  }

</style>

<?php

  include('include/variables.php');
  // $setup = file_get_contents($padExport1.'setup'.$padExport2);
  // $setup = 'abcd';
  $pregEnter = array();
  $pregExit = array();
  $set_projet = array();

  // $pregEnter[0] = '/\n/';
  // $pregExit[0] = '~~';

  // $sep = preg_replace($pregEnter, $pregExit, $setup);
  $set_projets = preg_split('/~~/', $setup);
  $set_projets = array_filter($set_projets);

  $set_name = array();
  $set_slug = array();
  $set_projetName = array();

  foreach($set_projets as $set_projet){
	 $explo = explode(' / ', $set_projet);
	 array_push($set_name, $explo[0]);
	 array_push($set_slug, $explo[1]);
	 array_push($set_projetName, $explo[2]);
  }



  $i=0;
  foreach($set_slug as $slug){

  $rand = rand(0, 10000);


  $pad_html = $padUrl.$slug.'_html';
  $pad_css = $padUrl.$slug.'_css';
  $pagi_odd = ($i+1 ) *2;
  $pagi_even = $pagi_odd + 1;

?>

<div class="pageG pages">
  <div class="cropTop"></div>
  <div class="cropLeft"></div>
  <iframe class="page gauche" src="pages.php?slug=<?= $slug ?>&amp;reload=off&amp;page=gauche<?php echo '&amp;rand='.$rand; ?>"></iframe>
  <div class="cropBottom"></div>
</div>

<div class="pageD pages">
  <div class="cropTop"></div>
  <div class="cropRight"></div>
  <iframe class="page droite" src="pages.php?slug=<?= $slug ?>&amp;reload=off&amp;page=droite<?php echo '&amp;rand='.$rand; ?>"></iframe>
  <div class="cropBottom"></div>
</div>

<?php
  $i++;
  }
?>
