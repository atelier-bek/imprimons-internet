<?php
$fileDir = 'fontes/';

  if (isset($_FILES['upFiles']) AND $_FILES['upFiles']['error'] == 0){
    // Testons si le fichier n'est pas trop gros
    if ($_FILES['upFiles']['size'] <= 1000000){
      // Testons si l'extension est autorisée
      $infosfichier = pathinfo($_FILES['upFiles']['name']);
      $extension_upload = $infosfichier['extension'];
      $extensions_autorisees = array('ttf', 'otf');
      if (in_array($extension_upload, $extensions_autorisees))
      {
      // On peut valider le fichier et le stocker définitivement
      move_uploaded_file($_FILES['upFiles']['tmp_name'],  $fileDir.'/'. basename($_FILES['upFiles']['name']));
      echo "<span style='color:green'>Le fichier a bien été envoyé!</span><br>";
}
    }
  }
?>

  <form  action="#" method="post"  enctype="multipart/form-data" >
    <div class="btn-file" style="width: 150px">
      <label for="file-input">Selectionner une fonte</label>
      <input id="file-input" type="file" name="upFiles" />
    </div>
    <div class="btn-submit" style="width: 20px">
      <label for="file-submit">Ok</label>
      <input id="file-submit" type="submit" value="save"/>
    </div>
  </form><br><br>
    <u>Formats -> .ttf et .otf</u><br><br>
  <ul>
  <?php

    $dir = $fileDir;

    if (is_dir($dir)) {
	if ($dh = opendir($dir)) {
	    while (($file = readdir($dh)) !== false) {
	      if( $file != '.' && $file != '..' && preg_match('#\.(ttf|otf)$#i', $file)) {
		?>
	       <li> <?php echo '<span style="color:grey" >'. $dir .'</span>'. $file; ?></li>

	    <?php
	  }
	    }
	    closedir($dh);
	}
    }
  ?>
  <ul>
