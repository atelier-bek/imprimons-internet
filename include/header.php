<!DOCTYPE html>
<html lang="fr">
	<head>
		<meta http-equiv="content-type" content="text/html; charset=utf-8" />
		<title>Web Print Party <?php if($nom) echo ' - '.$nom; ?></title>

		<?php
			if($page){
				echo '<link rel="stylesheet" type="text/css" media="screen" href="../css/page.css">';
				echo '<link rel="stylesheet" type="text/css" media="screen, print" href="css/page.css">';
			} else {
				echo '<link rel="stylesheet" type="text/css" media="screen" href="css/main.css">';
			}
		?>
		<script type="text/javascript" src="js/jquery-2.1.1.min.js"></script>
	</head>
  <body>
