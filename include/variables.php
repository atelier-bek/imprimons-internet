<?php

// variables globales

// ---------- instances de pad

$back = true;

if($back == true){

  $padUrl = 'https://semestriel.framapad.org/p/web_2_'; // framapad
  // $padUrl = 'https://titanpad.com/imprimons-internet-2-'; // titanpad
  // $padUrl = 'http://piratepad.net/imprimons-internet_2_'; // piratepad

  // framapad
  $padExport1 = 'https://semestriel.framapad.org/p/web_2_';
  $padExport2 = '/export/txt';
  // titanPad
  // $padExport1 = 'https://titanpad.com/ep/pad/export/imprimons-internet-2-';
  // $padExport2 = '/latest?format=txt';
  // Piratepad
  // $padExport1 = 'http://piratepad.net/ep/pad/export/imprimons-internet_2_';
  // $padExport2 = '/latest?format=txt';

}else{

  $padUrl = 'https://semestriel.framapad.org/p/web_'; // framapad
  // $padUrl = 'https://titanpad.com/imprimons-internet-'; // titanpad
  // $padUrl = 'http://piratepad.net/imprimons-internet_'; // piratepad
  // ---------- pads export

  // framapad
  $padExport1 = 'https://semestriel.framapad.org/p/web_';
  $padExport2 = '/export/txt';
  // titanPad
  // $padExport1 = 'https://titanpad.com/ep/pad/export/imprimons-internet-';
  // $padExport2 = '/latest?format=txt';
  // Piratepad
  // $padExport1 = 'http://piratepad.net/ep/pad/export/imprimons-internet_';
  // $padExport2 = '/latest?format=txt';

}

$setup = file_get_contents('https://semestriel.framapad.org/p/web_setup'.$padExport2);
