function reloadPreview(button, pageG, pageD){
    var srcG = pageG.attr('src');
    var srcD = pageD.attr('src');
  button.click(function(){


    var number = 1 + Math.floor(Math.random() * 10000);
    //
    pageG.removeAttr('src');
    pageD.removeAttr('src');
    pageG.attr('src', srcG+'&rand='+number);
    pageD.attr('src', srcD+'&rand='+number);

    // document.getElementById('pageG').contentDocument.location.reload(true);
    // document.getElementById('pageD').contentDocument.location.reload(true);

  })
}


function showPads(button, pads, start){
  if(start == true){
    pads.hide();
  }
  button.click(function(){
    if($(this).children().html() == 'OFF'){
      $(this).children().html('ON');
      pads.show();
    } else {
      $(this).children().html('OFF');
      pads.hide();
    }
  })
}


function zoom(sizer, pageG, pageD, pageGback, pageDback){
  var marginLeft;
  sizer.children('[type=range]').on('input change', function() {
    var value = $(this).val();
    value = value/100;
    if(pageGback.length > 0){
      pageGback.css({
        'transform': 'scale('+value+')',
      })
      pageDback.css({
        'transform': 'scale('+value+')',
      })
      marginLeft = pageGback.width()*(value);
      pageGback.parent().width(marginLeft);
      // pageDback.parent().width(marginLeft);
    }
    pageG.css({
      'transform': 'scale('+value+')',
    })
    pageD.css({
      'transform': 'scale('+value+')',
    })
    marginLeft = pageG.width()*(value);
    pageG.parent().width(marginLeft);
    pageD.parent().width(marginLeft);
    var totalW = pageG.parent().width()+pageD.parent().width();
    var windowW = $(window).width();
    if(totalW >= windowW){
      $('body').width(totalW+200);
    } else{
      $('body').width(windowW);
    }
  })
}

function zoomHome(sizer, double, doubleBack, pageG, pageD, pageGback, pageDback){
  var doubleW = double.width();
  var marginLeft;
  sizer.children('[type=range]').on('input change', function(){

    var value = $(this).val();
    value1 = value*7.37;
    value2 = value/200;

    double.width(value1);
    double.height(value1/1.41025);

    doubleBack.width(value1);
    doubleBack.height(value1/1.41025);

    pageG.css({
      'transform': 'scale('+value2+')'
    })
    pageD.css({
      'transform': 'scale('+value2+')'
    })

    pageGback.css({
      'transform': 'scale('+value2+')'
    })
    pageDback.css({
      'transform': 'scale('+value2+')'
    })

    marginLeft = pageG.width()*(value2);
    pageG.parent().width(marginLeft);
    pageD.parent().width(marginLeft);
    pageG.parent().height(marginLeft * 1.41025);
    pageD.parent().height(marginLeft * 1.41025);

    marginLeftBack = pageGback.width()*(value2);
    pageGback.parent().width(marginLeftBack);
    pageDback.parent().width(marginLeftBack);
    pageGback.parent().height(marginLeftBack * 1.41025);
    pageDback.parent().height(marginLeftBack * 1.41025);

    var margin = double.width();
    doubleBack.css({'margin-left': -margin-10})
  })

}


function posBackHome(doubleBack, double){
  console.log('gn,opz');
  var margin = double.width();
  doubleBack.css({'margin-left': -margin-10})
}
