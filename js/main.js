$(document).ready(function(){

  var iframeDoc     = 'https://annuel.framapad.org/p/imprimons-internet_doc';
  var reload        = $('#nav').children('.reload');
  var padsToggle    = $('#nav').children('.pads');
  var sizer         = $('#nav').children('.sizer');
  var edit_doc      = $('#doc').children('#edit_doc');

  var pads          = $('#pads');
  var double        = $('.double');
  var doubleBack    = $('.doubleBack');
  var pageGHome     = double.children('#pageGauche').children('.gauche');
  var pageDHome     = double.children('#pageDroite').children('.droite');
  var pageGHomeBack = doubleBack.children('#pageGauche').children('.gauche');
  var pageDHomeBack = doubleBack.children('#pageDroite').children('.droite');
  var pageG         = $('#pageGauche.single').children('.pages');
  var pageD         = $('#pageDroite.single').children('.pages');
  var pageGback     = $('#pageGaucheBack.single').children('.pages');
  var pageDback     = $('#pageDroiteBack.single').children('.pages');
  var page          = $('#pages');
  var docPad        = $('#doc').children('iframe');

  reloadPreview(reload, pageG, pageD);
  showPads(padsToggle, pads, false);
  // showPads(edit_doc, docPad, true);
  zoom(sizer, pageG, pageD, pageGback, pageDback);
  zoomHome(sizer, double, doubleBack, pageGHome, pageDHome, pageGHomeBack, pageDHomeBack);
  if (doubleBack.length > 0){
    posBackHome(doubleBack, double);
  }

  edit_doc.click(function(){

    if($('#cont_doc').children('iframe').attr('id') == 'iframe_doc'){
      $('#cont_doc').load('include/doc.php');
      $(this).html('Éditer');
    }else{
      $('#cont_doc').html($('<iframe/>', {
	id: 'iframe_doc',
	src: iframeDoc,
      }));
      $(this).html('Retour');
    }
  });


});
  // $('.Load').click(function(){
  //   // alert('salut');
  //   $('.pageGauche').location.reload(1);
  // });
