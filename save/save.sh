#!/bin/bash

for((;;))
do
  DATE=`date +%m-%d:%H.%M`
  mkdir $DATE
  URL='https://semestriel.framapad.org/p/web'

  curl $URL'_setup/export/txt' > $DATE/setup.txt
 
  PROJETS=$(cat $DATE/setup.txt)
  
  while IFS='~~' read -ra ADDR; do
   # echo $ADDR
   for i in "${ADDR[@]}"; do
     echo $i
     PROJET=$i
     IFS='/'
     set $PROJET
     NOM=$2
     SLUG=$2
     PROJET=$3
     SLUG=${SLUG//[[:space:]]/}
     NOM=${NOM//[[:space:]]/}
     echo $SLUG
     IFS=''
     echo $URL'_'$SLUG'_html/export/txt' >> $DATE/links.txt
     mkdir $DATE/$SLUG
     curl $URL'_'$SLUG'_html/export/txt' > $DATE/$SLUG/$SLUG.html
     curl $URL'_'$SLUG'_css/export/txt' > $DATE/$SLUG/$SLUG.css

   done
  done <<< "$PROJETS"
sleep 600
done
