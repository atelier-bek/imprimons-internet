<?php
  include('include/header.php');
  include('include/variables.php');
  $nom = $_GET['slug'];
  $pad_html = $padUrl.$nom.'_html';
  $pad_css = $padUrl.$nom.'_css';

  if($back == true){

    ?>

    <div class="backWrap">
      <div id="pageGaucheBack" class="single wrap back">
        <iframe class="pages gauche" id="pageG" src="back.php?slug=<?= $nom ?>&amp;reload=off&amp;page=gauche"></iframe>
      </div>
      <div id="pageDroiteBack" class="single wrap back">
        <iframe class="pages droite" id="pageG" src="back.php?slug=<?= $nom ?>&amp;reload=off&amp;page=droite"></iframe>
      </div>
    </div>

  <?php } ?>

  <div id="pageGauche" class="single wrap">
    <iframe class="pages gauche" id="pageG" src="pages.php?slug=<?= $nom ?>&amp;reload=off&amp;page=gauche<?php if($back == true) echo '&amp;bg=true' ?>" allowtransparency="true"></iframe>
  </div>
  <div id="pageDroite" class="single wrap">
    <iframe class="pages droite" id="pageD"  src="pages.php?slug=<?= $nom ?>&amp;reload=off&amp;page=droite<?php if($back == true) echo '&amp;bg=true' ?>" allowtransparency="true"></iframe>
  </div>

  <div id="pads">
    <iframe class="html" src="<?= $pad_html ?>"></iframe>
    <iframe class="css" src="<?= $pad_css ?>"></iframe>
  </div>

<?php
  include('include/nav.php');
  include('include/footer.php');
?>
